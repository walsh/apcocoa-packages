--
-- This file is part of the ApCoCoA package pool.
--
--   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
--
--   Authors: 2007 Daniel Heldt, 2009 - 2011 Jan Limbeck, 2023 Julian Danner
--
-- Visit http://apcocoa.org/ for more information regarding ApCoCoA.
-- Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
-- and known issues.
--
-- The ApCoCoA package pool is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License (version 3 or
-- later) as published by the Free Software Foundation. A copy of the full
-- licence may be found in the file COPYING in this directory.
--
-- The ApCoCoA package pool is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with the ApCoCoA package pool; if not, write to the Free Software
-- Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-- This file is part of the ApCoCoA package pool.
--

Package $apcocoa/numbasics         -- basics numerical routines, implemented in the ApCoCoAServer 

export skip;

Define About()
  Return " Author: Daniel Heldt, Jan Limbeck, Julian Danner ";
  Return " Port to CoCoA5: Julian Danner ";
EndDefine;

--------------------------------------------------------------------------

--copied from old coclib.cpkg from CoCoA4
Define ConvMat(FuncName, Matrix)
   CheckArgTypes([STRING], [FuncName]);
   If Not(Type(Matrix) = MAT) Then
    try Matrix := Mat(Matrix); uponerror E do 
      Error(FuncName + ": Input was not in matrix form and could not be converted to matrix form either.");
      Return False;
    endtry;
   EndIf;
   Return Matrix;
EndDefine;

--------------------------------------------------------------------------

Define SVD(M)
  M := ConvMat("Num.SVD", M);
  Computation := "SVD";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  --G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- SVD


--------------------------------------------------------------------------

Define SingularValues(M)
  M := ConvMat("Num.SingulatValues", M);
  Computation := "SingularValues";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- SingularValues

--------------------------------------------------------------------------

Define QR(M)
  M := ConvMat("Num.QR", M);
  Computation := "QR";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- QR

--------------------------------------------------------------------------

Define EigenValues(M)
  M := ConvMat("Num.EigenValues", M);
  Computation := "EigenValues";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- EigenValues

--------------------------------------------------------------------------

Define EigenValuesAndVectors(M)
  M := ConvMat("Num.EigenValuesAndVectors", M);
  Computation := "EigenValuesAndRightVectors";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- EigenValuesAndVectors

--------------------------------------------------------------------------

Define EigenValuesAndAllVectors(M)
  M := ConvMat("Num.EigenValuesAndAllVectors", M);
  Computation := "EigenValuesAndVectors";
  R := record[];
  G := $apcocoa/server.OperationCommunication(Computation, [Tagged(M,"DOUBLE_MAT")], R);
  Return G;  
EndDefine; -- EigenValuesAndAllVectors

--------------------------------------------------------------------------

Define LeastSquaresQR(...)
  ReturnResidualErr := False;
  ReturnConditionNums := False;
  
  If Not(Len(ARGV) IsIn 2..4) Then
    error("LeastSquaresQR: Wrong numer of parameters! Must be between 2 and 4.");
  EndIf;
  
  Mat := ARGV[1];
  Vec := ARGV[2];
  
  CheckArgTypes([MAT, MAT, BOOL, BOOL], [Mat, Vec, ReturnResidualErr, ReturnConditionNums]);
  
  If Len(ARGV) > 2 Then
    ReturnResidualErr := ARGV[3];
  EndIf;
  
  If Len(ARGV) > 3 Then
    ReturnConditionNums := ARGV[4];
  EndIf;
  
  If (ReturnResidualErr) Then
    ReturnResidualErr := 1;
  Else
    ReturnResidualErr := 0;
  EndIf;
  
  If (ReturnConditionNums) Then
    ReturnConditionNums := 1;
  Else
    ReturnConditionNums := 0;
  EndIf;  
  
  Mat := ConvMat("Num.LeastSquaresQR", Mat);
  Vec := ConvMat("Num.LeastSquaresQR", Vec);
  If Not(NumRows(Mat) = NumRows(Vec)) Then
    Error("Matrix and Vector need to have the same dimensions");
  EndIf;
  Computation := "LeastSquaresQR";
  Return $apcocoa/server.OperationCommunication(Computation, [Tagged(Mat,"DOUBLE_MAT"),
          Tagged(Vec,"DOUBLE_MAT"), Tagged([ReturnResidualErr, ReturnConditionNums],"INT_LIST")], record[]);
EndDefine; --LeastSquaresQR

--------------------------------------------------------------------------

EndPackage;


