--
-- This file is part of the ApCoCoA package pool.
--
--   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
--
--   Authors: 2020 Julian Danner
--
-- Visit http://apcocoa.org/ for more information regarding ApCoCoA.
-- Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems 
-- and known issues.
--
-- The ApCoCoA package pool is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License (version 3 or
-- later) as published by the Free Software Foundation. A copy of the full
-- licence may be found in the file COPYING in this directory.
--
-- The ApCoCoA package pool is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with the ApCoCoA package pool; if not, write to the Free Software
-- Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

Package $apcocoa/misc

export inject;
export Dec;

--Input: ring R with indets that only have strictly positive indices and no indet with name 'ring_'
--Output: record R_ such that one can access the original indets via R_.ind[idx_1,...,idx_n]
--Note: can only be used if strictly positive subscripts are used!!
--Note: tries to replace CoCoA-4's _using_, since now one can more easily access indets R_.ind.
--Note2: NOT OPTIMIZED BY ANY MEANS (runs through all indets multiple times!)
define inject(R)
  --check whether all subscripts are strictly positive:
  if min(Flatten([IndetSubscripts(ind) | ind in Indets(R)])) < 1 then
    error("Only use inject with rings that have strictly positive indet subscripts!");
  endif;


  inds:=Indets(R);
  indet_names := MakeSet([IndetName(ind) | ind in inds]);

  --create record with appropriate fields
  R_:=record[];
  foreach ind_name in indet_names do
    R_[ind_name] := [];
  endforeach;

  --create table of subscriptindices that are needed
  foreach ind in inds do
    append(ref R_[IndetName(ind)], IndetSubscripts(ind));
  endforeach;

  --adapt record s.t. access is correct:
  --helper function to create large enough lists
  recList:=func(dims)
    ImportByRef recList;
    if len(dims)=0 then return []; endif;
    if len(dims)=1 then 
      return NewList(dims[1]);
    else
      return NewList(dims[1], recList(last(dims,len(dims)-1)));
    endif;
  endfunc;

  --find max values for each dim:
  foreach ind_name in fields(R_) do
    idxs:=R_[ind_name];
    dim_idxs:=len(idxs[1]);
    max_dims := [ max([idx[i] | idx in idxs]) | i in 1..dim_idxs];
    --create list of appropriate dims:
    R_[ind_name] := recList(max_dims);
  endforeach;

  --sets L[idx] to val, where idx may be a list of idxs
  setValue := func(ref L, idx, val)
    ImportByRef setValue;
    if len(idx)=0 then --at the end of recursion
      L:=val;
    else
      L_:=L[idx[1]];
      setValue(ref L_, last(idx,len(idx)-1), val);
      L[idx[1]] := L_;
    endif;
  endfunc;

  --fill R_ with the variables from R
  foreach ind in inds do
    setValue(ref R_[IndetName(ind)], IndetSubscripts(ind), ind);
  endforeach;
  
  R_.ring_:=R;
  return R_;
enddefine;


--adapted from ApCoCoA-1.9.1 - coclib.cpkg5
-- pretty prints the object X up to D digits
-- X can be MAT, LIST, POLY, INT, RAT or iteration thereof
Define Dec(X,Digits)
  If Type(X)=INT Or Type(X)=RAT Then
    Return DecimalStr(X,Digits);
  EndIf;
  If Type(X)=LIST Then
    Return [Dec(Y,Digits) | Y In X];
  EndIf;
  If Type(X)=MAT Then
    Return Mat([Dec(X[I],Digits) | I In 1..Len(X)]);
  EndIf;
  If Type(X)=RINGELEM and IsPolyRing(RingOf(X)) Then
    C:=Coefficients(X);
    S:=Support(X);
    If Len(C)=0 Then Return "0" EndIf;
    PString:="";
    For I:=1 To Len(C) Do
      D:=OpenOString();
      If not(IsOne(S[I])) Then
        Print S[I] On D;
      EndIf;
      SIString:=close(D);
      PString:=PString+DecimalStr(C[I],Digits)+" "+
               SIString+" ";
      If I<Len(C) And C[I+1]>0 Then
        PString:=PString+"+";
      EndIf;
    EndFor;
    Return PString;
  EndIf;
  --if none of the above matches!
  return Sprint(X);
EndDefine;


endpackage; --misc.cpkg5
