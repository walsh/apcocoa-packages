#!/bin/bash
# script to create symbolic links s.t. apcocoa packages and binaries are loaded
# with the standard packages automatically

#find current path
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


#detect whether path to cocoa5-executable is given
if test -z "$1" 
then
  #no path to executable is given, check if one is available in PATH
  if command -v cocoa5 > /dev/null 2>&1; then
    #executable in path found!
    COCOA_EXECUTABLE="cocoa5"
  else
    #executable in path NOT found, aborting!
    echo "Error: No path to cocoa5 executable was given nor found in PATH. Packages could not installed."
    exit 1
  fi
else
  COCOA_EXECUTABLE=$1
fi


# find cocoa-package-path via CoCoAPackagePath()-function within cocoa5
PACKAGE_PATH=$(echo "print CocoaPackagePath();" | $COCOA_EXECUTABLE --no-prompt | tail -n 1)
echo $PACKAGE_PATH

#check if PACKAG_PATH is a dir and abort accordingly
if [ ! -d "$PACKAGE_PATH" ]; then
  echo "Error: unexpected output by cocoa5; default package path not found. Packages were not installed."
  echo "Note: check that your cocoa5 executable can start and receive input!"
  echo ""
  echo "retrived path was:"
  echo "$PACKAGE_PATH"
  exit 1
fi
  

#create symbolic links to packages
if [ -d "$PACKAGE_PATH/apcocoa" -o -d "$PACKAGE_PATH/binaries" -o -d "$PACKAGE_PATH/temp" ]; then
  # apcocoa dirs already exist, aborting!
  echo "Error: at least one of the directories already exists. Packages were not installed."
  echo "Note: There must not be directories named apcocoa, binaries and temp in your cocoa package path."
  exit 1
else
  # apcocoa dirs do not exist
  /bin/ln -s "$DIR/apcocoa"  "$PACKAGE_PATH/apcocoa"
  /bin/ln -s "$DIR/binaries" "$PACKAGE_PATH/binaries"
  mkdir "$PACKAGE_PATH/temp"
  chmod o+w "$PACKAGE_PATH/temp" #ensure that any user is allowed to create tmp files
  echo "Created symlinks to packages and binaries, and created temp folder."
fi
