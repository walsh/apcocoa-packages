##
## This file is part of the ApCoCoA package pool.
##
##   Copyright (c) ApCoCoA Project (Prof. Dr. Martin Kreuzer, Uni Passau)
##
##   Authors: 2020 Julian Danner
##
## Visit http://apcocoa.org/ for more information regarding ApCoCoA.
## Visit http://www.apcocoa.org/wiki/ApCoCoA:KnownIssues for bugs, problems
## and known issues.
##
## The ApCoCoA package pool is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License (version 3 or
## later) as published by the Free Software Foundation. A copy of the full
## licence may be found in the file COPYING in this directory.
##
## The ApCoCoA package pool is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with the ApCoCoA package pool; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##


#NOTE: this script needs to be put into the 'binaries' folder
#written by J Danner 2020 (partly based on scripts by F Walsh 2018/19)
#start this script inside a sage -python shell for faster startup times of sage

#instead of 
# 'from sage.all import *'
#we only import relevant libs
from sage.misc.all       import *         # takes a while
from sage.rings.all      import *


import re         # required for fast importing and
import itertools  # exporting of polynomials from cocoa5


#polys  <- List of strings representing polynomials in CoCoA-format
#indets <- list of strings representing the sage indets
#cocoa5_indets <- list of strings representing the cocoa5 indets
#output : polynomials in sage format
def toSagePolynomials(polys, indets, cocoa5_indets, order='degrevlex'):
    R = BooleanPolynomialRing(len(indets), names=indets, order=order)
    #substitute each cocoa5-variables with the corresponding sage-variables in the polys
    for i in range(0 ,len(indets)):
      polys = [p.replace(cocoa5_indets[i], indets[i]) for p in polys]
      #polys = list(map(lambda p: p.replace(cocoa5_indets[i], indets[i]), polys))
    #finalize, i.e. cast strings to boolean polynomials and adapt exponentiation symbol
    polys = [R(p.replace('^','**')) for p in polys]
    return polys

#polys  <- List of strings representing polynomials in Sage-format
#indets <- list of strings representing the sage indets
#cocoa5_indets <- list of strings representing the cocoa5 indets
#output : polynomials in sage format
def toCocoaPolynomials(polys, indets, cocoa5_indets):
    polys = [str(p).replace(' ','') for p in polys]
    #substitute each cocoa5-variables with the corresponding sage-variables in the polys
    for i in reversed(range(0 ,len(indets))): #reverse is needed to correctly map x10 to x[10] and not x[1]0
      polys = [p.replace(indets[i], cocoa5_indets[i]) for p in polys]
      #polys = list(map(lambda p: p.replace(variables[i], cocoa5_variables[i]), polys))
    #finalize, i.e. adapt the exponentiation symbol
    polys = [p.replace('**', '^') for p in polys]
    return polys

#cocoa5_indets <- list of strings of cocoa5_indets
#output: list of indets in sage format
def toSageIndets(cocoa5_indets):
    return [cind.replace('[','').replace(']','').replace(',','_') for cind in cocoa5_indets]

#src <- path to file
#output: list of cocoa5_indets
def loadAsStr(src):
    with open(src) as f:
        L = f.read().splitlines()
    return L

#L <- list of elements
#src <- path to file
#saves elements in L as strings in file 'src'
def saveAsStr(L, src):
    with open(src, "w") as f:
        for i, item in enumerate(L):
            f.write('%s' % item)
            #if i%10 == 0:
            # f.write('\n')
            if i<len(L)-1 :
              f.write(', ')



#example:
#cocoa5_indets = loadAsStr('tmp/tmp_interreduce_polys_indets')
#indets = toSageIndets(cocoa5_indets)
#cocoa5_polys = loadAsStr('tmp/tmp_interreduce_polys')
#polys = toSagePolynomials(cocoa5_polys, indets, cocoa5_indets)
#R=polys[0].ring()
#polys = ideal(polys).interreduced_basis()
#cocoa5_polys = toCocoaPolynomials(polys, indets, cocoa5_indets)
#saveAsStr(cocoa5_polys,'tmp/tmp_interreduce_polys')

