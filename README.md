# ApCoCoA-Packages

This repository contains the CoCoA packages that are part of the ApCoCoA project. You can find a detailed description of ApCoCoA
in our [wiki](https://apcocoa.uni-passau.de). There you also find [documentation](https://apcocoa.uni-passau.de/wiki/index.php?title=Category:ApCoCoA_Packages) on how to use the packages.

## Installation
* If you installed the ApCoCoA GUI from the [download page](https://apcocoa.uni-passau.de/wiki/index.php?title=Downloads) in the wiki, then you don't have to do anything. All the packages are already included. 

* If you prefer to use CoCoA from the command line, vim or emacs, then you can use our install script. Note that the script requires a 
working installation of [CoCoA](https://apcocoa.uni-passau.de/wiki/index.php?title=HowTo:Contribute_an_ApCoCoA-2_Package).

## How to Contribute
If you want to contribute a CoCoA package, you can create a pull request here in the repository. Alternatively you can send your code to
info@apcocoa.org.
Please also have a look at the wiki page [How to Contribute](https://apcocoa.uni-passau.de/wiki/index.php?title=HowTo:Contribute_an_ApCoCoA-2_Package).